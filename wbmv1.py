#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"

import os
import datetime

import click

import torch
from torch import nn
from torch.utils import data as torch_data

from lib import setup_logger, get_config
from lib.data import WBM, WBMTest

log = setup_logger("WBM-AI")
conf = get_config()


class DummyWriter:
    def __init__(self, *args, **kwargs):
       pass

    def add_scalar(self, *args, **kwargs):
        pass


EXP_NAME = "dsmall"


def get_model():
    """"
    This version of the wbm takes in a Tensor of (3, 1920, 1080) and has a label
    """
    # TODO: add dropout?
    return nn.Sequential(
        nn.Conv2d(3, 9, (12, 36)),
        nn.ReLU(),
        nn.Dropout2d(0.1),
        nn.Conv2d(9, 16, (6, 6)),
        nn.Dropout2d(0.1),
        nn.MaxPool2d((6, 6)),
        nn.ReLU(),
        nn.Conv2d(16, 8, (6, 6)),
        nn.Dropout2d(0.1),
        nn.ReLU(),
        nn.Conv2d(8, 4, (6, 6)),
        nn.MaxPool2d((3, 3)),
        nn.ReLU(),
        nn.Flatten(),
        nn.Linear(420, 56),
        nn.Dropout(0.2),
        nn.ReLU(),
        nn.Linear(56, 8),
        nn.ReLU(),
        nn.Linear(8, 1),
    )


class MaxSampler(torch_data.Sampler):
    def __init__(self, data_source, max_items):
        super().__init__(data_source)
        self.max_items = min(max_items, len(data_source))
        self.sampler = torch_data.SubsetRandomSampler(range(len(data_source)))
        # if max_items >= len(data_source):
        #     raise ValueError("Not supported")

    def __len__(self):
        return self.max_items

    def __iter__(self):
        return (a for a,_ in zip(self.sampler, range(self.max_items)))


def main(device="cpu", num_epochs=100, batch_size=12, num_workers=1, destdir="~/SIN/models"):
    try:
        from torch.utils.tensorboard import SummaryWriter
    except ImportError:
        log.info("Trouble with tensorboard import, not using")
        SummaryWriter = DummyWriter
    else:
        os.makedirs(conf["output"]["tensorboard"], exist_ok=True)
        log.info("Run tensorboard --logdir %s" % conf["output"]["tensorboard"])

    log.info(f"Workers: {num_workers}, batchsize: {batch_size}")

    destdir = os.path.expanduser(destdir)
    os.makedirs(destdir, exist_ok=True)

    log.info("Creating model on %s", device)
    model = get_model().to(device)

    w_train = WBM()
    # test_size = int(.2 * len(w))
    # w_test, w_train = torch_data.random_split(w, (test_size, len(w) - test_size))
    # adjust as necessary, depending on RAM et al
    trainloader = torch_data.DataLoader(
        w_train, batch_size=batch_size, shuffle=True, num_workers=num_workers, pin_memory=True)

    # Forward pass will happen many times, might as well cache
    w_test = WBMTest(cache=True)
    # disable all the data augmentations for the test_mode
    testloader = torch_data.DataLoader(
        w_test, num_workers=num_workers, batch_size=batch_size, pin_memory=True,
        sampler=MaxSampler(w_test, len(w_train)))

    tb_writer = SummaryWriter(conf["output"]["tensorboard"], flush_secs=30)
    runname = f"{EXP_NAME}_{datetime.datetime.now().strftime('%Y%m%dT%H:%M')}"

    lfunc = nn.BCEWithLogitsLoss()
    # optim = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9)
    optim = torch.optim.Adam(model.parameters())

    log.info("Setup complete, starting training")

    for epoch in range(num_epochs + 1):
        loss_agg = 0.0
        n_train = 0
        test_loss = 0.0
        n_test = 0
        model.train()
        for i, data in enumerate(trainloader):
            optim.zero_grad()

            images, labels = (d.to(device) for d in data)
            # labels = labels * .9 + 0.05
            labels = labels * 0.98 + 0.01  # label smoothing

            log.debug("Data Loaded")

            outputs = model(images)
            log.debug("Forward Pass")

            loss = lfunc(outputs.squeeze(-1), labels.float())
            log.debug(f"Loss Calculation {loss.item()}")

            loss.backward()
            log.debug("Backward Step")

            optim.step()
            log.debug("Optimisation update")

            loss_agg += loss * len(labels)
            n_train += len(labels)
            log.info(f"e: {epoch}, mb: {i}, l: {loss.item():.5f}")

        with torch.no_grad():
            # no backprop needed for the testset
            # this saves on memory and speeds up computations
            model.eval()
            for j, tdat in enumerate(testloader):
                images, labels, paths = tdat
                outputs = model(images.to(device))
                loss = lfunc(outputs.squeeze(-1), labels.float().to(device))
                test_loss += loss * len(labels)
                n_test += len(labels)
                log.info(f"(test) e: {epoch}, mb: {j}, l: {loss.item():.5f}")

        log.info(f"epoch: {epoch}, train_loss: {loss_agg / n_train:.5f}, "
                 f"test_loss = {test_loss / n_test:.5f}")
        tb_writer.add_scalars(runname, {"train": loss_agg / n_train,
                                        "test": test_loss / n_test}, epoch)

        # if True:
        if epoch % (num_epochs // 20) == 0:
            # save 20 times throughout the training
            fname = f"{destdir}/{EXP_NAME}_{epoch}"
            log.info(f"Writing to {fname}.pth")
            torch.save(model.state_dict(), f"{fname}.pth")


def check_forward(device):
    """
    This is a quick sanity check.
    """
    mod = get_model().to(device)
    w = WBM()
    im, lab = w[-1]
    log.info("Data loaded")
    log.info(f"img.size {im.shape}")
    o = mod(im.unsqueeze(0).to(device))
    log.info(f"o {o.shape}")
    if o.numel() == 1:
        lfunc = nn.BCEWithLogitsLoss()
        loss = lfunc(o.squeeze(0), torch.FloatTensor([lab]).to(device))
        log.info(f"loss: {loss}")


@click.command()
@click.option("--batch", default=conf["training"]["batch"])
@click.option("--workers", default=conf["training"]["workers"])
@click.option("--epochs", default=conf["training"]["epochs"])
@click.option("--test", is_flag=True)
@click.option("--cuda/--no-cuda", default=True)
@click.option("--destdir", default=conf["output"]["modelfile"])
def init(batch, workers, epochs, test, cuda, destdir):
    if cuda:
        # let's try and use it
        # Maybe not as a global variable?
        device = "cuda" if torch.cuda.is_available() else "cpu"
    else:
        device = "cpu"

    # Don't overwrite previous runs.
    dt = datetime.datetime.now().strftime("%Y%m%dT%H:%M")
    destdir = os.path.join(destdir, dt)
    os.makedirs(destdir, exist_ok=True)

    if not test:
        main(device, epochs, batch, workers, destdir)
    else:
        check_forward(device)


if __name__ == '__main__':
    init()
