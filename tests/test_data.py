#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"

import pytest

from lib.data import *


@pytest.fixture()
def data_train():
    return WBM()


@pytest.fixture()
def data_test():
    return WBMTest()


@pytest.fixture(params=[WBM, WBMTest])
def databm(request):
    return request.param()


def test_labels(databm):
    from collections import Counter
    c = Counter(databm.labels)
    assert c.keys() == {0, 1}
    print(c)


def test_len(databm):
    assert len(databm) > 0


#
def test_train_stats(data_train):
    from collections import Counter
    c = Counter(data_train.labels)
    assert 0.25 < c[1] / (c[0] + c[1]) < 0.75


def test_test_stat(data_test):
    from collections import Counter
    c = Counter(data_test.labels)
    assert c[1] / (c[0] + c[1]) < 0.3
