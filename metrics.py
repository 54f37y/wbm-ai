#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"

import os

import click
import numpy as np
from sklearn import metrics
from matplotlib import pyplot as plt
import pandas as pd
from lib import get_config

plt.style.use("ggplot")
conf = get_config()


@click.command()
@click.argument("predfile", nargs=-1)
def main(predfile):
    plt.figure(figsize=(14, 7))

    for fl in predfile:
        df = pd.read_csv(fl)
        yhat = df.prediction
        labs = df.label

        if labs.all() or not labs.any():
            # i.e. are they all positive or are they all negative
            print(f"\tSkipping {fl} due to invalid auc")
            continue

        # courtesy of sklearn
        fpr, tpr, _ = metrics.roc_curve(labs, yhat)
        roc_auc = metrics.auc(fpr, tpr)
        #
        print(f"{fl} : {roc_auc}")
        # print("FPR: ", fpr)
        # print("TPR", tpr)

        plt.plot(fpr, tpr,
                 lw=2, label='%s (AUC = %0.2f)' % (os.path.basename(fl), roc_auc))
        plt.legend(loc="lower right")

    plt.xlim([-0.02, 1.02])
    plt.ylim([-0.02, 1.02])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC')
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.show()


if __name__ == '__main__':
    main()
