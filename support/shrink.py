__author__ = "Aiden Rohde <aiden@54f37y.com>"

import os
import click
from PIL import Image


@click.command()
@click.argument("input_dir")
@click.option("--shrinkage", default=0.2,
              help="Portion of the original size of the image the output " +
                   "should be. E.g. 0.2 (20%) of a 1920x1080 image is 384x216")
@click.option("--file_type", default=".jpg",
              help="Which files should we look for. Defaults to .jpg")
@click.option("--output_dir", default="./output",
              help="Portion of the original size of the image the output " +
                   "should be. E.g. 0.2 (20%) of a 1920x1080 image is 384x216")
@click.option("--force", is_flag=True, default=False,
              help="Overwrite instead of skipping")
@click.option("--dryrun", is_flag=True, default=False,
              help="Report and do nothing else")
@click.option("--verbose", is_flag=True, help="Print details of shrinking.")
def main(input_dir, shrinkage, file_type, output_dir, force, dryrun, verbose):
    """Takes the given directory and shrinks all the images it finds there
    down to a portion of their original size. Defaults to 20%.
    """
    # make the output directory if it doesn't exist
    input_dir = os.path.expanduser(input_dir)
    output_dir = os.path.expanduser(output_dir)
    os.makedirs(output_dir, exist_ok=True)

    for file in os.listdir(input_dir):  # loop thru the given directory
        if file.endswith(file_type):  # find the files with the right suffix
            in_fl = os.path.join(input_dir, file)
            out_f = os.path.join(output_dir, file)

            if dryrun:
                print(f"Shrink {in_fl} to {shrinkage} of original and write to"
                      f" {out_f}")
                continue

            if os.path.isfile(out_f) and not force:
                if verbose:
                    print(f"Skipping {out_f} since it already exists")
                continue
            try:
                img = Image.open(in_fl)

                # calculate the output size
                size = (img.width * shrinkage, img.height * shrinkage)

                img.thumbnail(size)
                img.save(out_f)
            except OSError as e:
                print(f"Error {e}")
            except Exception as e:
                print("Cannot shrink %s" % file)
                print(e)
            else:
                if verbose:
                    print("Shrunk %s" % file)


if __name__ == '__main__':
    main()
