#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"

"""
When I labelled the images, i did it on a shrunken version
This just allows me to resize from 384 x 216 to 1920 x 1080
"""
import click
import os

from xml.etree import ElementTree as ET


def is_unprocessed(boxdict):
    return boxdict["width"] <= 216 and boxdict["xmax"] <= 384


@click.command()
@click.argument("labelpath")
@click.option("--run", is_flag=True)
def main(labelpath, run):
    for dpath, _, fls in os.walk(labelpath):
        for f in fls:
            xml_path = os.path.join(dpath, f)
            print(f"Looking at {xml_path}")
            tree = ET.parse(xml_path)
            xml = tree.getroot()

            size = {sub.tag: int(sub.text) for sub in xml.find("size")}

            scale = 1
            if size["width"] != 1920 or size["height"] != 1080:
                scale = 1920 / size["width"]
            else:
                print(f"{xml_path} looks updated already")
                continue

            for dim in xml.find("size"):
                if dim.tag == "depth":
                    continue
                dim.text = str(int(dim.text) * 5)

            for sub in xml.find("object").find("bndbox"):
                val = int(int(sub.text) * scale)

                if sub.tag == "xmin" and val < 50:
                    val = 0
                elif sub.tag == "xmax" and val > 1920 - 50:
                    val = 1920

                sub.text = str(val)
                print(f"{sub.tag} : {sub.text}")

            # print(str(ET.tostring(xml, "unicode")))
            # ans = input("Look good? [Y/n]")
            # if ans is '' or ans.lower() == "y":
            tree.write(xml_path)



if __name__ == '__main__':
    main()
