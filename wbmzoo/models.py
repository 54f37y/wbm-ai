#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"
"""
This is where we define the model zoo. Match the function name
to the .pth file and we can then be guaranteed that the forward pass will 
always work.

This is primarily due to the fact
"""
import os

import torch
from torch import nn


def baseline():
    """"
    This version of the wbm takes in a Tensor of (3, 1920, 1080)
    with a single label output
    """
    return nn.Sequential(
        nn.Conv2d(3, 9, (12, 12)),
        nn.MaxPool2d((2, 6)),
        nn.ReLU(),
        nn.Conv2d(9, 4, (6, 6)),
        nn.MaxPool2d((5, 5)),
        nn.ReLU(),
        nn.Flatten(),
        nn.Linear(836, 90),
        nn.ReLU(),
        nn.Linear(90, 15),
        nn.ReLU(),
        nn.Linear(15, 1),
    )


def regularized():
    """"
    This version of the wbm takes in a Tensor of (3, 1920, 1080) and has a label
    """
    return nn.Sequential(
        nn.Conv2d(3, 9, (12, 36)),
        nn.ReLU(),
        nn.Dropout2d(0.1),
        nn.Conv2d(9, 16, (6, 6)),
        nn.Dropout2d(0.1),
        nn.MaxPool2d((6, 6)),
        nn.ReLU(),
        nn.Conv2d(16, 8, (6, 6)),
        nn.Dropout2d(0.1),
        nn.ReLU(),
        nn.Conv2d(8, 4, (6, 6)),
        nn.MaxPool2d((3, 3)),
        nn.ReLU(),
        nn.Flatten(),
        nn.Linear(420, 56),
        nn.Dropout(0.2),
        nn.ReLU(),
        nn.Linear(56, 8),
        nn.ReLU(),
        nn.Linear(8, 1),
    )


def get_model(name):
    """
    Convenience method that gets the model and it's parameters from the zoo

    Args:
        name (str): the name to request

    Returns:
        torch.Module

    """

    model_params = torch.load(
        os.path.join(os.path.dirname(__file__), f"{name}.pth"),
        map_location=torch.device("cpu"))
    # print(globals().keys())
    model = globals()[name]()
    model.load_state_dict(model_params)
    return model
