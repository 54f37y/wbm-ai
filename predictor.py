#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"
"""
The prediction machine

This takes in a model file and a directory
"""
import csv
import os
import math

import torch
from torch.utils.data import DataLoader
import click

from lib import get_config, setup_logger
from lib.data import WBMTest

conf = get_config()
log = setup_logger("prediction")


# TODO: setup_logger fails if there are not existing log files on Windows.
#  Can we generate those?

@click.command()
@click.option("--model", default="baseline")
@click.option("--datadir", "--dir", default=conf["data"]["path"])
@click.option("--outfile", "-o", default=conf["output"]["prediction"])
@click.option("--cuda/--no-cuda", default=True)
@click.option("--verbose", "-v", count=True)
def main(model, datadir, outfile, cuda, verbose):
    os.makedirs(os.path.dirname(os.path.expanduser(outfile)), exist_ok=True)
    if cuda:
        device = "cuda" if torch.cuda.is_available() else "cpu"
    else:
        device = "cpu"

    if os.path.isfile(os.path.expanduser(model)):
        # assume we're testing a live model
        log.info(f"Reading params of {model} into the network defined in wbmv1.py")
        from wbmv1 import get_model
        m = get_model()
        params = torch.load(os.path.expanduser(model),
                            map_location=torch.device('cpu'))
        m.load_state_dict(params)
    else:
        log.info("Loading '%s' from model zoo", model)
        from wbmzoo import get_model
        m = get_model(model)

    # this disables all the stuff like dropout and batchnorm
    # that regularize during training. Even if we don't have anything
    # we should do this as a standard thing
    m.eval()
    ds = WBMTest(datadir, cache=False)
    w = DataLoader(ds, batch_size=conf["prediction"]["batch"],
                   shuffle=False, num_workers=conf["prediction"]["workers"])
    log.info("Using device %s", device)
    m = m.to(device)

    nbatches = math.ceil(len(ds)/conf["prediction"]["batch"])

    with torch.no_grad():
        # this reduces memory consumption and speeds up forward mode
        log.info("Writing predictions to %s", outfile)
        with open(outfile, 'w') as f:
            outcsv = csv.writer(f)
            outcsv.writerow(["file", "label", "prediction"])
            for batchnum, (im, lab, pth) in enumerate(w, 1):
                y = torch.sigmoid(m(im.to(device))).cpu()
                log.info(f"Ran batch {batchnum} of {nbatches} of size {y.size()[0]} forward")
                for pred, l, name in zip(y, lab, pth):
                    if verbose:
                        log.info(f"{name}, label: {l}, {100 * pred.item():.2f}")
                    outcsv.writerow((name, l.item(), pred.item()))
                log.info(f"Written {y.size()[0]} lines to file {outfile}")
    log.info("Finished Prediction")


if __name__ == '__main__':
    main()
