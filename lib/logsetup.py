#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"

"""
Copied from: https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/

Just import this file when you need to use logs
"""
import logging.config
import os

import yaml


def handle_rel_path(handledict):
    try:
        path = os.path.expanduser(handledict["filename"])
    except KeyError:
        return
    else:
        os.makedirs(os.path.dirname(path), exist_ok=True)
        handledict["filename"] = path


def setup_logger(name, config=None):
    if not config:
        config = os.path.join(os.path.dirname(__file__), "logging.yaml")

    if os.path.exists(config):
        with open(config, 'rt') as f:
            config = yaml.safe_load(f.read())

        for handle in config["handlers"].values():
            handle_rel_path(handle)

        logging.config.dictConfig(config)
    else:
        print("No logging config, falling back to default")
        logging.basicConfig(level=logging.INFO)

    return logging.getLogger(name)
