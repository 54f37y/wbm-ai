# WBM AI

See [wbmzoo](tree/master/wbmzoo) for details on the models

## File Reference

1. `wbmwv1.py` - this is the training script - i.e. this allows you to create new models. 
2. `predictor.py` - this is the forward pass, i.e. it is used to predict the presence of wbm. 
3. `metrics.py` - this allows you to compare two or more models based on their predictions.
    - This plots ROCs and reports their AUC. Higher AUC is better, but we need to optimise our FPR and TPR 
    - This does **NOT** check that the csv's passed in are based on identical files
4. `Metrics.ipynb` - an interactive metrics investigation to aid investigations

## Config

1. `default_config.yaml` - this controls data input and output locations as well as training settings
2. `lib/logging.yaml` - Edit to change logging locations and level

## New Model Release

1. Copy New Model architecture to `wbmzoo/models.py` and the param file to `wbmzoo/{newname}.pth` 
    - If the new model is not backwards compatible, add a git hash to README.md for the old models and delete the older files 
    and architectures going forward.
    - Add a date so we can compare what the dataset looked like at the time for training. Definitely list any systemic changes
2. Test you can use the new model from `predictor.py` with it's `{newname}` as the model argument
3. Copy the changes from (1) to `data-processing` for a release. 

## Python Requirements

1. torch & torchvision - the neural nets
    1. On Windows you'll need to download these using a [special link 
    from the website](https://pytorch.org/get-started/locally/). 
    Regular pip install doesn't work. 
    2. If you do not have a GPU, be sure to select the NONE option for CUDA
2. PIL - the image lib
    1. On Windows this is simply installed with pip 
3. click - the cmdline lib
    1. On Windows this is simply installed with pip
 4. The virtual environment
    1. On Windows this requires you to all RemoteSigned scripts to be run. The 
    powershell command is `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned`
    2. More details on that [here](https://docs.microsoft.com/en-au/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-6)




