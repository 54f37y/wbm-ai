#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"

import os
import yaml

confdict = None


def get_config():
    """
    #TODO: merge a custom dictionary
    Returns:

    """
    global confdict
    if not confdict:
        c = os.path.join(os.path.dirname(__file__), "default_config.yaml")
        try:
            with open(c) as f:
                confdict = yaml.safe_load(f)
        except OSError as e:
            print(e)

    for toplevel in confdict.values():
        try:
            for k, v in toplevel.items():
                if isinstance(v, str) and v.startswith(("~", "/")):
                    path = os.path.expanduser(v)
                    if not os.path.exists(path):
                        os.makedirs(path)
                    toplevel[k] = path
        except (TypeError, AttributeError):
            pass

    return confdict
