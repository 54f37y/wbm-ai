#!/usr/bin/env bash

# This is a script to convert the images which are 1920 x 1080 into
# 384 x 216 (i.e. a 5th

# rename the pl_test files
# rename -n 's/^\ +\((.*)\)/$1/g' *
# zeropad the strings
# rename 's/\d+/sprintf("%05d",$&)/e' foo*

# eg command
#convert -resize 50% myfigure.png

currdir=$(pwd)

cd ~/SIN/ || exit
mkdir -p data_small
cd data || exit


find . -maxdepth 4 -type d -exec mkdir -p "../data_small/{}" \;

# copy the folder structure across
#for i in $(find . -maxdepth 4 -type d)
#do
#    mkdir -p "../data_small/$i"
#done

find . -iname "*.jpg" -exec convert -resize 20% {} "../data_small/{}" \;

#for i in $(find . -iname "*.jpg")
#do
#    dest="../data_small/$i"
##    mkdir -p $(dirname $dest)
#    echo "convert -resize 20% $i $dest"
#    convert -resize 20% $i $dest
#done

cd "$currdir" || exit

# convert -resize 20% ./pf_wbm/1/1/1pf20190509193619060000.jpg ../data_small/./pf_wbm/1/1/1pf20190509193619060000.jpg