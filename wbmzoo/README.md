# Model Zoo

This model zoo is where I store trained models with varying performance.
This readme will list all the various models in reverse order of being added

ROC for Current Test Set (as of 11/01/2019)

![ROC](roc.png)



## Notes 

Use `wbmzoo.get_model` to grab the model for your forward pass code. 
When you add a model, add it's model config as code to  `wbmzoo/models.py`  
  
## regularized

Added: 4/11/2019

This was to try and improve performance over the baseline

- 100 epochs of Adam on the small images.
- Comparisons to a test set (which is sort of small) to check on the likelihood of overfitting
- Dropout and deeper net to try and fit better without overfitting

Performance is surprisingly excellent. 


## baseline

Added 28/10/19

This was the proof of concept whether a simple convnet could work 
for the amount of data. Surprisingly, it was surprisingly 
effective and I've included this as a simple baseline.

Problems include: 

- Overtrained - 100 epochs of ADAM
- Limited robustness - no dropout or weight penalty
- No training set

This takes an image shrunk 20% so 384 * 216 
(shrunk from 1920 x 1080) and passes it through the network. 
There is no sigmoid activation, so you'll need to `.sigmoid()`
to put the output between 0 and 1


