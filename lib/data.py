#  -*- coding: utf-8 -*-
__author__ = "Varun Nayyar <nayyarv@gmail.com>"

import logging
import os
import tempfile
import atexit
import shutil

from xml.etree import ElementTree as ET

import attr
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms as trf

from lib import get_config

log = logging.getLogger(__name__)
conf = get_config()


def to_rgb(image):
    """convenience method to avoid having to write this again"""
    return Image.open(image).convert("RGB")


@attr.s(frozen=True)
class LazyPreprocess:
    """
    A class that takes an iterable and preprocess and caches it lazily.
    This is due to the fact that resizing needs to be done once but can be kind of slow
    This might also be useful for other preprocessing that needs to be done once

    Args:
        imgs: list of imagepaths
        preproc: torchvision.Transform to be applied as pre-processing
        delete_after: delete the files afterwards
    """
    imgs = attr.ib()
    preproc = attr.ib()
    delete_after = attr.ib(default=True)

    cachedir = attr.ib(init=False,
                       factory=lambda: tempfile.mkdtemp(prefix="/usr/scratch/tmp/"))

    def __attrs_post_init__(self):
        if self.delete_after:
            # clears the cache file on exit. Since it creates a new dir each time
            # this is only necessary for debugging
            atexit.register(shutil.rmtree, self.cachedir)

        os.makedirs(self.cachedir, exist_ok=True)
        log.info("Writing temp files to %s", self.cachedir)

    def __len__(self):
        return len(self.imgs)

    def _cache_path(self, item):
        ftype = os.path.splitext(self.imgs[item])[1]
        return os.path.join(self.cachedir, f"{item}{ftype}")

    def __getitem__(self, item):
        im_cache_name = self._cache_path(item)

        try:
            im = to_rgb(im_cache_name)
        except FileNotFoundError:
            im = self.preproc(to_rgb(self.imgs[item]))
            pth = self._cache_path(item)
            log.debug("Saving %s to %s", self.imgs[item], pth)
            im.save(pth)

        return im


class WBM(Dataset):
    IMG_SUFFIX = tuple(conf["data"]["filetype"])

    # TODO v2
    # Let's chop the image up slightly. We remove the top 1/3rd and bit off
    # the bottom. This will allow us to chop the image up more

    # We hardcode the image resize, and assume an input of
    # 1080p so this resize works as expected.
    # This step is slow and is best cached for any kind of training
    CACHE_TRANSFORM = [
        trf.Resize((216, 384))
    ]

    # define these transforms here so other classes can use them easily
    TRAIN_TRANSFORM = [
        # add some jitter to the image
        trf.ColorJitter(),
        # Make some gray so we're not too dependent on colour
        trf.RandomGrayscale(),
        # Augment with flips, we especially want to do this
        # with terminals
        trf.RandomHorizontalFlip(),
    ]

    # these should be applied even in test mode.
    # these are the base transforms with the above transforms used in addition
    # as data augment/regularization methods
    TEST_TRANSFORM = [

        trf.ToTensor(),
    ]

    def __init__(self, root=conf["data"]["path"]):
        """
        A Pytorch esque class for handling the dataload and transform
        This simply labels them as 1, 0 for wbm or not
        Args:
            root: path pointing to the data dir  s3://54f37y-wbm-dataset/
        """
        root = os.path.expanduser(os.path.join(root, "pl_wbm"))
        imgs = []
        self.labels = []

        for lab, dtype in enumerate(["negative", "positive"]):
            d_root = os.path.join(root, dtype)
            log.info(f"Reading '{d_root}' with label {lab}")
            try:
                for dpath, _, fls in os.walk(d_root):
                    if not fls:
                        log.info(f"No images found in {dpath}")
                        continue
                    imglist = [os.path.join(dpath, f) for f in fls if f.endswith(WBM.IMG_SUFFIX)]
                    if not imglist:
                        log.error(f"maybe filter is bad? {fls[0]}")
                    imgs += imglist
                    self.labels += [lab] * len(imglist)
                    log.info(f"Found {len(imglist)} files in {dpath}")
            except OSError as e:
                log.error(e)
        self.transform = None
        self.train()
        self.imgs = LazyPreprocess(imgs, trf.Compose(WBM.CACHE_TRANSFORM))

    def train(self, mode=True):
        if mode:
            self.transform = trf.Compose(WBM.TRAIN_TRANSFORM + WBM.TEST_TRANSFORM)
        else:
            self.transform = trf.Compose(WBM.TEST_TRANSFORM)

    def eval(self):
        self.train(False)

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, item):
        im = self.transform(self.imgs[item])
        return im, self.labels[item]


def parse_xml(path):
    """
    Read the xml of the file and get the path it's referring.
    In this case, we assume that if a label exists, it's positive
    """
    try:
        root = ET.parse(path).getroot()
    except OSError as e:
        log.error(e)
    else:
        assert root[2].tag == "path"
        return root[2].text


class WBMTest(Dataset):
    """
    A version for the test data since it's mixed and manually labelled
    """

    def __init__(self, root=conf["data"]["path"], cache=False):
        # hardcode test/1 till I define cleans and skips
        imroot = os.path.join(os.path.expanduser(root), "pl_wbm", "test")
        log.info(f"Looking in {imroot} for images")
        imgs = []

        try:
            for dpath, folders, fls in os.walk(imroot):
                if not fls and not folders:
                    # this is likely an error
                    log.info(f"No images or folders found in {dpath}")
                    continue
                imglist = [os.path.join(dpath, f) for f in fls if f.endswith(WBM.IMG_SUFFIX)]
                imgs += imglist
                log.info(f"Found {len(imglist)} files in {dpath}")
        except OSError as e:
            log.error(e)

        labelroot = os.path.expanduser(os.path.join(root, "pl_wbm", "label", "test_label"))
        self.iswbm = set()

        try:
            for dpath, _, fls in os.walk(labelroot):
                fls = [f for f in fls if f.endswith("xml")]
                for xml_file in fls:
                    self.iswbm.add(parse_xml(os.path.join(dpath, xml_file)))
                log.info(f"Read {len(fls)} labels in {dpath}")

        except OSError as e:
            log.error(e)

        # sort for clarity
        self.ogfiles = sorted(imgs)
        if cache:
            log.info("Using lazy caching on preprocessing")
            self.imgs = LazyPreprocess(self.ogfiles, trf.Compose(WBM.CACHE_TRANSFORM))
            # No augmenting transforms on the test
            self.transform = trf.Compose(WBM.TEST_TRANSFORM)
        else:
            log.info("Not caching the preprocessing step")
            self.imgs = self.ogfiles
            self.transform = trf.Compose(
                [trf.Lambda(to_rgb)] + WBM.CACHE_TRANSFORM + WBM.TEST_TRANSFORM)

        log.info("Finished Collecting Test Images")

    def __len__(self):
        return len(self.imgs)

    @property
    def labels(self):
        """Keeping the same interface as wbm"""
        return [1 if self.ogfiles[i] in self.iswbm else 0 for i in range(len(self))]

    def __getitem__(self, item):
        lab = self.ogfiles[item] in self.iswbm
        im = self.transform(self.imgs[item])
        return im, 1 if lab else 0, self.ogfiles[item]


